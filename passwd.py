#!/usr/bin/env python
import random

# On génère les listes utilisées pour générer le mot de passe
minuscules = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
majuscules = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
chiffres = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
cara_spec = ["?", "!", "-", "_", "/", "%", "=", "+", "^", "*", "@", "&", "(", ")", "[", "]"]
accents = ["à", "é", "è", "ù", "ê", "ô", "î", "ï", "ü", "û", "ö", "ä", "â"]
# Initialisation des variables
prog = True
advance = True

print("Welcome !")

# On définie la fonction qui génère les passwords
def genere_passwd(longueur,chaine):
    passwd = [] # Initialise une liste vide
    for i in range(0,longueur): # Boucle de 0 à la longueur du mot de passe précisé par l'utilisateur
        x = random.randint(0,len(chaine)-1) # Génère un nb aléatoire entre 0 et la taille max de la chaine
        passwd.append(chaine[x]) # Concatène l'élément aléatoire (x) de la liste à chaque itération de la boucle
    passwd = "".join(passwd) # On termine par transformer la liste en chaine de caractères
    return passwd

while prog: # Permet de générer rapidement un mot de passe complexe avec des paramètres préremplis
    fast = input("Voulez-vous générer rapidement un mot de passe complexe ? (O/N) ?\n(renseignez N pour accéder au mode avancé ou Q pour quitter) : ")
    if fast != "O" and fast != "o" and fast != "N" and fast != "n" and fast != "Q" and fast != "q":
        print("Vous devez répondre par O (oui), N (non / mode avancé) ou Q (quitter)")
        print("Bye !")
        prog = False
        advance = False
    elif fast == "O" or fast == "o":
        longueur = 24
        chaine = minuscules + majuscules + chiffres + cara_spec
        passwd = genere_passwd(longueur,chaine)
        print(f"\nLe mot de passe complexe généré est : {passwd}\n")
    elif fast == "Q" or fast == "q":
        print("Bye !")
        prog = False
        advance = False
    else:
        prog = False
    
while advance: # Permet de générer un mot de passe en questionnant l'utilisateur
    longueur = 0
    difficulte = 0
    chaine = []
    
    while True:
        try:
            longueur = int(input("\nQuelle est la longueur du mot de passe souhaité ? "))
            print("\n========= Niveaux de complexité disponibles =========\n\
            1 : Lettres minuscules uniquement\n\
            2 : Lettres minuscules et majuscules\n\
            3 : Lettres minuscules, majuscules et chiffres\n\
            4 : Lettres minuscules, majuscules, chiffres et caractères spéciaux\n\
            5 : Lettres minuscules, majuscules, chiffres, caractères spéciaux et accents\n")
            difficulte = int(input("Quel est le niveau de complexité souhaité ? "))
            break
        except ValueError:
            print("")
            print("======================= ATTENTION ! =====================")
            print("Vous devez répondre à la question avec des chiffres !")
            print("")    
    
    if difficulte < 1 or difficulte > 5:
        print("Erreur: le niveau de complexité sélectionné n'existe pas !\nBye !")
        break
    elif difficulte == 1:
        chaine = minuscules
    elif difficulte == 2:
        chaine = minuscules + majuscules
    elif difficulte == 3:
        chaine = minuscules + majuscules + chiffres
    elif difficulte == 4:
        chaine = minuscules + majuscules + chiffres + cara_spec
    elif difficulte == 5:
        chaine = minuscules + majuscules + chiffres + cara_spec + accents

    passwd = genere_passwd(longueur,chaine)

    print(f"\nLe mot de passe généré est : {passwd}\n")
    new = input("Voulez-vous générer un nouveau mot de passe ? (O/N) ")
    if new == "N" or new == "n":
        advance = False
        print("Bye !")