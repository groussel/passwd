# Passwd

Script Python 3 permettant de générer des mots de passe ou passphrase avec différents niveaux de complexité.

# Fonctionnement

Il y a deux modes de fonctionnement : 
*  *Mode rapide* : permet de générer rapidement un mot de passe complexe (100% de robustesse [PasswordMeter](http://www.passwordmeter.com))
*  *Mode avancé* : l'utilisateur sélectionne la longueur et la complexité du mot de passe

L'application démarre par le mode rapide, si l'utilisateur refuse, il entre dans le mode avancé.

# Evolutions possibles
*  Intégrer une API de vérification de complexité de mot de passe (type [PasswordMeter](http://www.passwordmeter.com))
*  *... ouvert aux propositions*